import { Place } from './Place';

let uniqid = require('uniqid');

export class Reservation {
    /**
     * @param {uniqid} uniqid
     * @param {date} date
     * @param {integer} nbVisitor
     * @param {Place} place
     */
    constructor(place, date) {
        this.id = uniqid();
        this.date = date;
        this.nbVisitor = 0; // ?? Why not put it in class Place because of the limitVisitor ??
        this.place = place;
    }
}