import { Place } from './Place';
import { Reservation } from './Reservation';

let uniqid = require('uniqid');

export class User {
    /**
     * @param {uniqid} uniqid
     * @param {string} name user's name
     * @param {integer} nbVisit not really util for now but increment it at each reservation !!
    //  * @param {integer} age not util for now !!
     * @param {boolean} isMuseumStaff
     */
    constructor(name, isMuseumStaff) {
        this.id = uniqid();
        this.name = name;
        this.isMuseumStaff = isMuseumStaff;
        this.nbVisit = 0;
    }
    doReservation(place, date) {
        // add a new instance of Reservation with the place and the date wanted (increment nbVisitor in );
        let newReservation = new Reservation(place, date);
        newReservation.nbVisitor++;
        this.nbVisit++;
        return newReservation;
    }
}