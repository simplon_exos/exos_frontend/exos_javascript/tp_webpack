let uniqid = require('uniqid');

export class Place {
    /**
     * @param {integer} uniqid
     * @param {string} placeName
     * @param {integer} limitVisitor
     * @param {date} calendar will be an array of free dates
     */
    constructor(placeName) {
        this.id = uniqid();
        this.place = placeName;
        this.limitVisitor = 100;
        this.calendar = ['21/09/2019', '22/09/2019'];
    }
}