import { User } from "./User";
import { Place } from "./Place";

let toto = new User('Toto', true);
console.log(toto);
let pierrot = new User('Pierrot', false);
console.log(pierrot);
let jeannot = new User('Jeannot', false);
console.log(jeannot);


let guignolMuseum = new Place('Guignol Museum');
console.log(guignolMuseum);

let opera = new Place('Opéra de Lyon');
console.log(opera);


console.log(toto.doReservation(opera, '22/09/2019'));
console.log(toto);